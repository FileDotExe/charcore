package xyz.filedotexe.core.util;

import java.util.Random;

/**
 * Created by Ervin on 1/25/2017.
 */
public class Util {

    public static int getRandom(int lower, int upper) {
        return new Random().nextInt(upper - lower + 1) + lower;
    }
}
